<?php

namespace Drupal\consultancy_availability\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\consultancy_services\Available\ConsultancyAvailableDate;

/**
 * Form controller for Consultancy availability edit forms.
 *
 * @ingroup consultancy_availability
 */
class ConsultancyAvailabilityForm extends ContentEntityForm
{

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;


  /**
   * ConsultancyAvailabilityForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface|NULL $entity_type_bundle_info
   * @param \Drupal\Component\Datetime\TimeInterface|NULL $time
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    TimeInterface $time = NULL,
    AccountProxyInterface $account
  )
  {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('current_user'),
      $container->get('consultancy_services.available_date')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /* @var \Drupal\consultancy_availability\Entity\ConsultancyAvailability $entity */
    $form = parent::buildForm($form, $form_state);


    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $entity = $this->entity;
    $form_state->getValue('new_revision');

    // Should go into a test
    $request = [
      'user' => '',
      'type' => '',
      'start_date' => '',
      'end_date' => '',
    ];

    $objDA = new ConsultancyAvailableDate();
    $result = $objDA->getDateAvailability($request);
    if ($result['result'] === false) {
      $this->messenger()->addMessage($result['message']);
      return '';
    } else {
      $this->messenger()->addMessage($result['message']);
    }
    //end of should be in a test


    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') !== FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->account->id());
    } else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label Consultancy availability.', [
            '%label' => $entity->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label Consultancy availability.', [
            '%label' => $entity->label(),
          ]));
    }
    $form_state->setRedirect('entity.consultancy_availability.canonical', ['consultancy_availability' => $entity->id()]);
  }

}
