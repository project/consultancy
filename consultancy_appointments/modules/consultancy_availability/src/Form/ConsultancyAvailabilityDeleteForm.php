<?php

namespace Drupal\consultancy_availability\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Consultancy availability.
 *
 * @ingroup consultancy_availability
 */
class ConsultancyAvailabilityDeleteForm extends ContentEntityDeleteForm {


}
