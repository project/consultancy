<?php

namespace Drupal\consultancy_availability;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Consultancy availability.
 *
 * @see \Drupal\consultancy_availability\Entity\ConsultancyAvailability.
 */
class ConsultancyAvailabilityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\consultancy_availability\Entity\ConsultancyAvailabilityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished consultancy availability entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published consultancy availability entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit consultancy availability entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete consultancy availability entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add consultancy availability entities');
  }

}
