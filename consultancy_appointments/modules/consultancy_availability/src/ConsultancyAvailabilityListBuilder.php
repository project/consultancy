<?php

namespace Drupal\consultancy_availability;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Consultancy availability.
 *
 * @ingroup consultancy_availability
 */
class ConsultancyAvailabilityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\consultancy_availability\Entity\ConsultancyAvailability $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.consultancy_availability.edit_form',
      ['consultancy_availability' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
