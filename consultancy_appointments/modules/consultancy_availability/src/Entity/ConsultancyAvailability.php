<?php

namespace Drupal\consultancy_availability\Entity;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the Consultancy availability entity.
 *
 * @ingroup consultancy_availability
 *
 * @ContentEntityType(
 *   id = "consultancy_availability",
 *   label = @Translation("Consultancy availability"),
 *   handlers = {
 *     "storage" =
 *   "Drupal\consultancy_availability\ConsultancyAvailabilityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\consultancy_availability\ConsultancyAvailabilityListBuilder",
 *     "views_data" =
 *   "Drupal\consultancy_availability\Entity\ConsultancyAvailabilityViewsData",
 *     "translation" =
 *   "Drupal\consultancy_availability\ConsultancyAvailabilityTranslationHandler",
 *
 *     "form" = {
 *       "default" =
 *   "Drupal\consultancy_availability\Form\ConsultancyAvailabilityForm",
 *       "add" =
 *   "Drupal\consultancy_availability\Form\ConsultancyAvailabilityForm",
 *       "edit" =
 *   "Drupal\consultancy_availability\Form\ConsultancyAvailabilityForm",
 *       "delete" =
 *   "Drupal\consultancy_availability\Form\ConsultancyAvailabilityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" =
 *   "Drupal\consultancy_availability\ConsultancyAvailabilityHtmlRouteProvider",
 *     },
 *     "access" =
 *   "Drupal\consultancy_availability\ConsultancyAvailabilityAccessControlHandler",
 *   },
 *   base_table = "consultancy_availability",
 *   data_table = "consultancy_availability_field_data",
 *   revision_table = "consultancy_availability_revision",
 *   revision_data_table = "consultancy_availability_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer consultancy availability",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/consultancy/consultancy_availability/{consultancy_availability}",
 *     "add-form" = "/admin/consultancy/consultancy_availability/add",
 *     "edit-form" =
 *   "/admin/consultancy/consultancy_availability/{consultancy_availability}/edit",
 *     "delete-form" =
 *   "/admin/consultancy/consultancy_availability/{consultancy_availability}/delete",
 *     "version-history" =
 *   "/admin/consultancy/consultancy_availability/{consultancy_availability}/revisions",
 *     "revision" =
 *   "/admin/consultancy/consultancy_availability/{consultancy_availability}/revisions/{consult_availability_revision}/view",
 *     "revision_revert" =
 *   "/admin/consultancy/consultancy_availability/{consultancy_availability}/revisions/{consult_availability_revision}/revert",
 *     "revision_delete" =
 *   "/admin/consultancy/consultancy_availability/{consultancy_availability}/revisions/{consult_availability_revision}/delete",
 *     "translation_revert" =
 *   "/admin/consultancy/consultancy_availability/{consultancy_availability}/revisions/{consult_availability_revision}/revert/{langcode}",
 *     "collection" = "/admin/consultancy/consultancy_availability",
 *   },
 *   field_ui_base_route = "consultancy_availability.settings"
 * )
 */
class ConsultancyAvailability extends RevisionableContentEntityBase implements ConsultancyAvailabilityInterface
{

  use EntityChangedTrait;
  use EntityPublishedTrait;


  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values)
  {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel)
  {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    } elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage)
  {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the consultancy_availability owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name)
  {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime()
  {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp)
  {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner()
  {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId()
  {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid)
  {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account)
  {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields = parent::baseFieldDefinitions($entity_type);


    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);


    // User ID
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Created by'))
      ->setDescription(t('The user ID of the creator.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    //Name of times Consultant can be available
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Days Consultant is availability.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);


    // Date Consultant is available to work
    $fields['date'] = BaseFieldDefinition::create('daterange')
      ->setLabel(t('Date and time'))
      ->setDescription(t('Availability of Consultant.'))
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'daterange_datelist',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
     // ->setDefaultValue($rangeDefaults);


    //Making the time repeatable if needed
    $fields['repeatability'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Does availability need to be repeated?'))
      ->setDescription(t('  '))
      ->setSettings([
        'allowed_values' => [
          'none' => 'Not Needed',
          'weekly' => 'Yes Weekly',
          'fortnightly' => 'Yes Fortnightly',
          'triweekly' => 'Yes Triweekly',
          'monthly' => 'Yes Monthly',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    //Does the consultant want to make this an all day event
    $fields['all_day'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is this an all day event?'))
      ->setSetting('on_label', t(''))
      ->setDisplayOptions('form', array(
        'type' => 'boolean',
        'weight' => '0',
      ))
      ->setDisplayConfigurable('form', TRUE);


    //Extra text if needed
    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setSettings([
        'default_value' => '',
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['status']->setDescription(t('A boolean indicating whether the Consultant is going to display in calendar.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);


    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time it was created.'));


    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that it was last edited.'));


    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);


    return $fields;
  }

}
