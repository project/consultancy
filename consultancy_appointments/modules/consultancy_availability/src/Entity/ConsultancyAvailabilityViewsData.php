<?php

namespace Drupal\consultancy_availability\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Consultancy availability.
 */
class ConsultancyAvailabilityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
