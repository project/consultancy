<?php

namespace Drupal\consultancy_availability\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Consultancy availability.
 *
 * @ingroup consultancy_availability
 */
interface ConsultancyAvailabilityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Consultancy availability name.
   *
   * @return string
   *   Name of the Consultancy availability.
   */
  public function getName();

  /**
   * Sets the Consultancy availability name.
   *
   * @param string $name
   *   The Consultancy availability name.
   *
   * @return \Drupal\consultancy_availability\Entity\ConsultancyAvailabilityInterface
   *   The called Consultancy availability.
   */
  public function setName($name);

  /**
   * Gets the Consultancy availability creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Consultancy availability.
   */
  public function getCreatedTime();

  /**
   * Sets the Consultancy availability creation timestamp.
   *
   * @param int $timestamp
   *   The Consultancy availability creation timestamp.
   *
   * @return \Drupal\consultancy_availability\Entity\ConsultancyAvailabilityInterface
   *   The called Consultancy availability.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Consultancy availability revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Consultancy availability revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\consultancy_availability\Entity\ConsultancyAvailabilityInterface
   *   The called Consultancy availability.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Consultancy availability revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Consultancy availability revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\consultancy_availability\Entity\ConsultancyAvailabilityInterface
   *   The called Consultancy availability.
   */
  public function setRevisionUserId($uid);

}
