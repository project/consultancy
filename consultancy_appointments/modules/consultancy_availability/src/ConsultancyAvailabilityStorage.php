<?php

namespace Drupal\consultancy_availability;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\consultancy_availability\Entity\ConsultancyAvailabilityInterface;

/**
 * Defines the storage handler class for Consultancy availability.
 *
 * This extends the base storage class, adding required special handling for
 * Consultancy availability.
 *
 * @ingroup consultancy_availability
 */
class ConsultancyAvailabilityStorage extends SqlContentEntityStorage implements ConsultancyAvailabilityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ConsultancyAvailabilityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {consultancy_availability_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {consultancy_availability_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(ConsultancyAvailabilityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {consultancy_availability_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('consultancy_availability_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
