<?php

namespace Drupal\consultancy_availability\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\consultancy_availability\Entity\ConsultancyAvailabilityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConsultancyAvailabilityController.
 *
 *  Returns responses for Consultancy availability routes.
 */
class ConsultancyAvailabilityController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new ConsultancyAvailabilityController.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Consultancy availability revision.
   *
   * @param int $consultancy_availability_revision
   *   The Consultancy availability revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($consultancy_availability_revision) {
    $consultancy_availability = $this->entityTypeManager()->getStorage('consultancy_availability')
      ->loadRevision($consultancy_availability_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('consultancy_availability');

    return $view_builder->view($consultancy_availability);
  }

  /**
   * Page title callback for a Consultancy availability revision.
   *
   * @param int $consultancy_availability_revision
   *   The Consultancy availability revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($consultancy_availability_revision) {
    $consultancy_availability = $this->entityTypeManager()->getStorage('consultancy_availability')
      ->loadRevision($consultancy_availability_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $consultancy_availability->label(),
      '%date' => $this->dateFormatter->format($consultancy_availability->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Consultancy availability.
   *
   * @param \Drupal\consultancy_availability\Entity\ConsultancyAvailabilityInterface $consultancy_availability
   *   A Consultancy availability object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ConsultancyAvailabilityInterface $consultancy_availability) {
    $account = $this->currentUser();
    $consultancy_availability_storage = $this->entityTypeManager()->getStorage('consultancy_availability');

    $langcode = $consultancy_availability->language()->getId();
    $langname = $consultancy_availability->language()->getName();
    $languages = $consultancy_availability->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $consultancy_availability->label()]) : $this->t('Revisions for %title', ['%title' => $consultancy_availability->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all consultancy availability revisions") || $account->hasPermission('administer consultancy availability entities')));
    $delete_permission = (($account->hasPermission("delete all consultancy availability revisions") || $account->hasPermission('administer consultancy availability entities')));

    $rows = [];

    $vids = $consultancy_availability_storage->revisionIds($consultancy_availability);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\consultancy_availability\ConsultancyAvailabilityInterface $revision */
      $revision = $consultancy_availability_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $consultancy_availability->getRevisionId()) {
          $link = $this->l($date, new Url('entity.consultancy_availability.revision', [
            'consultancy_availability' => $consultancy_availability->id(),
            'consultancy_availability_revision' => $vid,
          ]));
        }
        else {
          $link = $consultancy_availability->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.consultancy_availability.translation_revert', [
                'consultancy_availability' => $consultancy_availability->id(),
                'consultancy_availability_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.consultancy_availability.revision_revert', [
                'consultancy_availability' => $consultancy_availability->id(),
                'consultancy_availability_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.consultancy_availability.revision_delete', [
                'consultancy_availability' => $consultancy_availability->id(),
                'consultancy_availability_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['consultancy_availability_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
