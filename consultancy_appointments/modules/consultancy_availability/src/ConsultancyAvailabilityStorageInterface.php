<?php

namespace Drupal\consultancy_availability;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\consultancy_availability\Entity\ConsultancyAvailabilityInterface;

/**
 * Defines the storage handler class for Consultancy availability.
 *
 * This extends the base storage class, adding required special handling for
 * Consultancy availability.
 *
 * @ingroup consultancy_availability
 */
interface ConsultancyAvailabilityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Consultancy availability revision IDs for a specific Consultancy availability.
   *
   * @param \Drupal\consultancy_availability\Entity\ConsultancyAvailabilityInterface $entity
   *   The Consultancy availability.
   *
   * @return int[]
   *   Consultancy availability revision IDs (in ascending order).
   */
  public function revisionIds(ConsultancyAvailabilityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Consultancy availability author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Consultancy availability revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\consultancy_availability\Entity\ConsultancyAvailabilityInterface $entity
   *   The Consultancy availability.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ConsultancyAvailabilityInterface $entity);

  /**
   * Unsets the language for all Consultancy availability with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
