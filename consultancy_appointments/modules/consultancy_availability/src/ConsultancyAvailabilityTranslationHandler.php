<?php

namespace Drupal\consultancy_availability;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for consultancy_availability.
 */
class ConsultancyAvailabilityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
