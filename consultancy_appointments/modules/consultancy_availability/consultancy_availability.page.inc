<?php

/**
 * @file
 * Contains consultancy_availability.page.inc.
 *
 * Page callback for Consultancy availability.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Consultancy availability templates.
 *
 * Default template: consultancy_availability.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_consultancy_availability(array &$variables) {
  // Fetch ConsultancyAvailability Entity Object.
  $consultancy_availability = $variables['elements']['#consultancy_availability'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
