<?php

namespace Drupal\consultancy_services\Available;


use Drupal\consultancy_services\Utilities\Validation;

  /**
 * Class AvailableDate.
 */
class ConsultancyAvailableDate implements ConsultancyAvailableDateInterface {

  /**
   * Constructs a new AvailableDate object.
   */
  public function __construct() {

  }


  /**
   * @inheritDoc
   */
  public function getDateAvailability(array $configuration):array {

    $required = ['user', 'type', 'start_date', 'end_date'];
    if(Validation::arrayKeyValidation($required,$configuration)) {
        return [
          'result' => true,
          'message' => 'This is true',
        ];
    }

    return [
      'result' => false,
      'message' => 'The is false',
    ];
  }

}
