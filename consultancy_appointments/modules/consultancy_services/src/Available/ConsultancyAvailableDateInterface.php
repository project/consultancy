<?php

namespace Drupal\consultancy_services\Available;



/**
 * Interface AvailableInterface.
 */
interface ConsultancyAvailableDateInterface {

  /**
   * This class is designed to error catch $configuration
   * service requests and to disseminate the allocation of
   * resources to find the result.
   *
   *
   * @param array $configuration
   *     - User who is requesting the date query
   *     - Consultancy entity date type to be queried
   *     - Receives dates that are being queried
   *
   * @example $configuration = [
   *   'user'=> '',
   *   'type' => '',
   *   'start_date'=> '',
   *   'end_date' => ''
   * ];
   *
   * @return array
   *   Returns an array that has,
   *    1. boolean does this date or time already exist on this entity
   *    2. String one is for messaging to give the user or develop feedback
   *
   * @example $configuration = [
   *   'result'=> '', // true or false
   *   'message' => '', // message
   * ];
   *
   */
  public function getDateAvailability(array $configuration);
}
