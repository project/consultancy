<?php

namespace Drupal\consultancy_services\Utilities;

/**
 * Class Required
 *
 * @package Drupal\consultancy_services\Utilities
 */
class Validation {

  /**
   * @param array $required
   *   Array of values that represent the keys that are required
   * @param array $request
   *   Array to be tested to see if has the required keys
   *
   * @return bool
   */
  public static function arrayKeyValidation(array $required, array $request){

    if(!array_diff_key(array_flip($required), $request)){
       return true;
    }

    return false;
  }

}
