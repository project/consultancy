<?php

/**
 * @file
 * Contains consultancy_type.page.inc.
 *
 * Page callback for Consultancy type entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Consultancy type templates.
 *
 * Default template: consultancy_type.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_consultancy_type(array &$variables) {
  // Fetch ConsultancyType Entity Object.
  $consultancy_type = $variables['elements']['#consultancy_type'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
