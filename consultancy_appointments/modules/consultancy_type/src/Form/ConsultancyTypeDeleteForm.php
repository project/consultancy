<?php

namespace Drupal\consultancy_type\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Consultancy type entities.
 *
 * @ingroup consultancy_type
 */
class ConsultancyTypeDeleteForm extends ContentEntityDeleteForm {


}
