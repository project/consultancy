<?php

namespace Drupal\consultancy_type;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Consultancy type entity.
 *
 * @see \Drupal\consultancy_type\Entity\ConsultancyType.
 */
class ConsultancyTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\consultancy_type\Entity\ConsultancyTypeInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished consultancy type entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published consultancy type entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit consultancy type entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete consultancy type entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add consultancy type entities');
  }

}
