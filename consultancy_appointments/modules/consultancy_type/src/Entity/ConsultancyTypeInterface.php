<?php

namespace Drupal\consultancy_type\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Consultancy type entities.
 *
 * @ingroup consultancy_type
 */
interface ConsultancyTypeInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Consultancy type name.
   *
   * @return string
   *   Name of the Consultancy type.
   */
  public function getName();

  /**
   * Sets the Consultancy type name.
   *
   * @param string $name
   *   The Consultancy type name.
   *
   * @return \Drupal\consultancy_type\Entity\ConsultancyTypeInterface
   *   The called Consultancy type entity.
   */
  public function setName($name);

  /**
   * Gets the Consultancy type creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Consultancy type.
   */
  public function getCreatedTime();

  /**
   * Sets the Consultancy type creation timestamp.
   *
   * @param int $timestamp
   *   The Consultancy type creation timestamp.
   *
   * @return \Drupal\consultancy_type\Entity\ConsultancyTypeInterface
   *   The called Consultancy type entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Consultancy type revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Consultancy type revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\consultancy_type\Entity\ConsultancyTypeInterface
   *   The called Consultancy type entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Consultancy type revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Consultancy type revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\consultancy_type\Entity\ConsultancyTypeInterface
   *   The called Consultancy type entity.
   */
  public function setRevisionUserId($uid);

}
