<?php

namespace Drupal\consultancy_type\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Consultancy type entity.
 *
 * @ingroup consultancy_type
 *
 * @ContentEntityType(
 *   id = "consultancy_type",
 *   label = @Translation("Consultancy type"),
 *   handlers = {
 *     "storage" = "Drupal\consultancy_type\ConsultancyTypeStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\consultancy_type\ConsultancyTypeListBuilder",
 *     "views_data" =
 *   "Drupal\consultancy_type\Entity\ConsultancyTypeViewsData",
 *     "translation" =
 *   "Drupal\consultancy_type\ConsultancyTypeTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\consultancy_type\Form\ConsultancyTypeForm",
 *       "add" = "Drupal\consultancy_type\Form\ConsultancyTypeForm",
 *       "edit" = "Drupal\consultancy_type\Form\ConsultancyTypeForm",
 *       "delete" = "Drupal\consultancy_type\Form\ConsultancyTypeDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\consultancy_type\ConsultancyTypeHtmlRouteProvider",
 *     },
 *     "access" =
 *   "Drupal\consultancy_type\ConsultancyTypeAccessControlHandler",
 *   },
 *   base_table = "consultancy_type",
 *   data_table = "consultancy_type_field_data",
 *   revision_table = "consultancy_type_revision",
 *   revision_data_table = "consultancy_type_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer consultancy type entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/consultancy/consultancy_type/{consultancy_type}",
 *     "add-form" = "/admin/consultancy/consultancy_type/add",
 *     "edit-form" =
 *   "/admin/consultancy/consultancy_type/{consultancy_type}/edit",
 *     "delete-form" =
 *   "/admin/consultancy/consultancy_type/{consultancy_type}/delete",
 *     "version-history" =
 *   "/admin/consultancy/consultancy_type/{consultancy_type}/revisions",
 *     "revision" =
 *   "/admin/consultancy/consultancy_type/{consultancy_type}/revisions/{consultancy_type_revision}/view",
 *     "revision_revert" =
 *   "/admin/consultancy/consultancy_type/{consultancy_type}/revisions/{consultancy_type_revision}/revert",
 *     "revision_delete" =
 *   "/admin/consultancy/consultancy_type/{consultancy_type}/revisions/{consultancy_type_revision}/delete",
 *     "translation_revert" =
 *   "/admin/consultancy/consultancy_type/{consultancy_type}/revisions/{consultancy_type_revision}/revert/{langcode}",
 *     "collection" = "/admin/consultancy/consultancy_type",
 *   },
 *   field_ui_base_route = "consultancy_type.settings"
 * )
 */
class ConsultancyType extends RevisionableContentEntityBase implements ConsultancyTypeInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the consultancy_type owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Consultancy type entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Consultancy type entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);


    $fields['time'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('What is the consult type times length'))
      ->setDescription(t('In hour and minutes'))
      ->setSettings([
        'precision' => 5,
        'scale' => 2,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['status']->setDescription(t('A boolean indicating whether the Consultancy type is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
