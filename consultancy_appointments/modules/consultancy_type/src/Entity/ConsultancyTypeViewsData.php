<?php

namespace Drupal\consultancy_type\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Consultancy type entities.
 */
class ConsultancyTypeViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
