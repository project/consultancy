<?php

namespace Drupal\consultancy_type;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\consultancy_type\Entity\ConsultancyTypeInterface;

/**
 * Defines the storage handler class for Consultancy type entities.
 *
 * This extends the base storage class, adding required special handling for
 * Consultancy type entities.
 *
 * @ingroup consultancy_type
 */
interface ConsultancyTypeStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Consultancy type revision IDs for a specific Consultancy type.
   *
   * @param \Drupal\consultancy_type\Entity\ConsultancyTypeInterface $entity
   *   The Consultancy type entity.
   *
   * @return int[]
   *   Consultancy type revision IDs (in ascending order).
   */
  public function revisionIds(ConsultancyTypeInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Consultancy type author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Consultancy type revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\consultancy_type\Entity\ConsultancyTypeInterface $entity
   *   The Consultancy type entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ConsultancyTypeInterface $entity);

  /**
   * Unsets the language for all Consultancy type with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
