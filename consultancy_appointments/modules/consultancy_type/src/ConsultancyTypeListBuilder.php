<?php

namespace Drupal\consultancy_type;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Consultancy type entities.
 *
 * @ingroup consultancy_type
 */
class ConsultancyTypeListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Consultancy type ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\consultancy_type\Entity\ConsultancyType $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.consultancy_type.edit_form',
      ['consultancy_type' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
