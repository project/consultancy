<?php

namespace Drupal\consultancy_type;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\consultancy_type\Entity\ConsultancyTypeInterface;

/**
 * Defines the storage handler class for Consultancy type entities.
 *
 * This extends the base storage class, adding required special handling for
 * Consultancy type entities.
 *
 * @ingroup consultancy_type
 */
class ConsultancyTypeStorage extends SqlContentEntityStorage implements ConsultancyTypeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ConsultancyTypeInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {consultancy_type_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {consultancy_type_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(ConsultancyTypeInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {consultancy_type_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('consultancy_type_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
