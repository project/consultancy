<?php

namespace Drupal\consultancy_type\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\consultancy_type\Entity\ConsultancyTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConsultancyTypeController.
 *
 *  Returns responses for Consultancy type routes.
 */
class ConsultancyTypeController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new ConsultancyTypeController.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Consultancy type revision.
   *
   * @param int $consultancy_type_revision
   *   The Consultancy type revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($consultancy_type_revision) {
    $consultancy_type = $this->entityTypeManager()->getStorage('consultancy_type')
      ->loadRevision($consultancy_type_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('consultancy_type');

    return $view_builder->view($consultancy_type);
  }

  /**
   * Page title callback for a Consultancy type revision.
   *
   * @param int $consultancy_type_revision
   *   The Consultancy type revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($consultancy_type_revision) {
    $consultancy_type = $this->entityTypeManager()->getStorage('consultancy_type')
      ->loadRevision($consultancy_type_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $consultancy_type->label(),
      '%date' => $this->dateFormatter->format($consultancy_type->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Consultancy type.
   *
   * @param \Drupal\consultancy_type\Entity\ConsultancyTypeInterface $consultancy_type
   *   A Consultancy type object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ConsultancyTypeInterface $consultancy_type) {
    $account = $this->currentUser();
    $consultancy_type_storage = $this->entityTypeManager()->getStorage('consultancy_type');

    $langcode = $consultancy_type->language()->getId();
    $langname = $consultancy_type->language()->getName();
    $languages = $consultancy_type->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $consultancy_type->label()]) : $this->t('Revisions for %title', ['%title' => $consultancy_type->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all consultancy type revisions") || $account->hasPermission('administer consultancy type entities')));
    $delete_permission = (($account->hasPermission("delete all consultancy type revisions") || $account->hasPermission('administer consultancy type entities')));

    $rows = [];

    $vids = $consultancy_type_storage->revisionIds($consultancy_type);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\consultancy_type\ConsultancyTypeInterface $revision */
      $revision = $consultancy_type_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $consultancy_type->getRevisionId()) {
          $link = $this->l($date, new Url('entity.consultancy_type.revision', [
            'consultancy_type' => $consultancy_type->id(),
            'consultancy_type_revision' => $vid,
          ]));
        }
        else {
          $link = $consultancy_type->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.consultancy_type.translation_revert', [
                'consultancy_type' => $consultancy_type->id(),
                'consultancy_type_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.consultancy_type.revision_revert', [
                'consultancy_type' => $consultancy_type->id(),
                'consultancy_type_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.consultancy_type.revision_delete', [
                'consultancy_type' => $consultancy_type->id(),
                'consultancy_type_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['consultancy_type_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
