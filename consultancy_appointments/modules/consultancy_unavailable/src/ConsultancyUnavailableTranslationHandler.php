<?php

namespace Drupal\consultancy_unavailable;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for consultancy_unavailable.
 */
class ConsultancyUnavailableTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
