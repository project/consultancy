<?php

namespace Drupal\consultancy_unavailable\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Consultancy unavailable entities.
 *
 * @ingroup consultancy_unavailable
 */
class ConsultancyUnavailableDeleteForm extends ContentEntityDeleteForm {


}
