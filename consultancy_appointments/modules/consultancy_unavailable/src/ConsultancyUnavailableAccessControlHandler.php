<?php

namespace Drupal\consultancy_unavailable;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Consultancy unavailable entity.
 *
 * @see \Drupal\consultancy_unavailable\Entity\ConsultancyUnavailable.
 */
class ConsultancyUnavailableAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\consultancy_unavailable\Entity\ConsultancyUnavailableInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished consultancy unavailable entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published consultancy unavailable entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit consultancy unavailable entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete consultancy unavailable entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add consultancy unavailable entities');
  }

}
