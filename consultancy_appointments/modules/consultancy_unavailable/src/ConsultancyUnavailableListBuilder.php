<?php

namespace Drupal\consultancy_unavailable;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Consultancy unavailable entities.
 *
 * @ingroup consultancy_unavailable
 */
class ConsultancyUnavailableListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Consultancy unavailable ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\consultancy_unavailable\Entity\ConsultancyUnavailable $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.consultancy_unavailable.edit_form',
      ['consultancy_unavailable' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
