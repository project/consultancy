<?php

namespace Drupal\consultancy_unavailable\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Consultancy unavailable entity.
 *
 * @ingroup consultancy_unavailable
 *
 * @ContentEntityType(
 *   id = "consultancy_unavailable",
 *   label = @Translation("Consultancy unavailable"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\consultancy_unavailable\ConsultancyUnavailableListBuilder",
 *     "views_data" = "Drupal\consultancy_unavailable\Entity\ConsultancyUnavailableViewsData",
 *     "translation" = "Drupal\consultancy_unavailable\ConsultancyUnavailableTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\consultancy_unavailable\Form\ConsultancyUnavailableForm",
 *       "add" = "Drupal\consultancy_unavailable\Form\ConsultancyUnavailableForm",
 *       "edit" = "Drupal\consultancy_unavailable\Form\ConsultancyUnavailableForm",
 *       "delete" = "Drupal\consultancy_unavailable\Form\ConsultancyUnavailableDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\consultancy_unavailable\ConsultancyUnavailableHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\consultancy_unavailable\ConsultancyUnavailableAccessControlHandler",
 *   },
 *   base_table = "consultancy_unavailable",
 *   data_table = "consultancy_unavailable_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer consultancy unavailable entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/consultancy/consultancy_unavailable/{consultancy_unavailable}",
 *     "add-form" = "/admin/consultancy/consultancy_unavailable/add",
 *     "edit-form" = "/admin/consultancy/consultancy_unavailable/{consultancy_unavailable}/edit",
 *     "delete-form" = "/admin/consultancy/consultancy_unavailable/{consultancy_unavailable}/delete",
 *     "collection" = "/admin/consultancy/consultancy_unavailable",
 *   },
 *   field_ui_base_route = "consultancy_unavailable.settings"
 * )
 */
class ConsultancyUnavailable extends ContentEntityBase implements ConsultancyUnavailableInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Consultancy unavailable entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Consultancy unavailable entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);





    $fields['date'] = BaseFieldDefinition::create('daterange')
      ->setLabel(t('Date and time'))
      ->setDescription(t('When the activity takes place.'))
      ->setRequired(false)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'daterange_default',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
      //->setDefaultValue($rangedefaults);

     $fields['status']->setDescription(t('A boolean indicating whether the Consultancy unavailable is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);


    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
