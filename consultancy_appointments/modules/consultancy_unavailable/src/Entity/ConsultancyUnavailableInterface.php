<?php

namespace Drupal\consultancy_unavailable\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Consultancy unavailable entities.
 *
 * @ingroup consultancy_unavailable
 */
interface ConsultancyUnavailableInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Consultancy unavailable name.
   *
   * @return string
   *   Name of the Consultancy unavailable.
   */
  public function getName();

  /**
   * Sets the Consultancy unavailable name.
   *
   * @param string $name
   *   The Consultancy unavailable name.
   *
   * @return \Drupal\consultancy_unavailable\Entity\ConsultancyUnavailableInterface
   *   The called Consultancy unavailable entity.
   */
  public function setName($name);

  /**
   * Gets the Consultancy unavailable creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Consultancy unavailable.
   */
  public function getCreatedTime();

  /**
   * Sets the Consultancy unavailable creation timestamp.
   *
   * @param int $timestamp
   *   The Consultancy unavailable creation timestamp.
   *
   * @return \Drupal\consultancy_unavailable\Entity\ConsultancyUnavailableInterface
   *   The called Consultancy unavailable entity.
   */
  public function setCreatedTime($timestamp);


}
