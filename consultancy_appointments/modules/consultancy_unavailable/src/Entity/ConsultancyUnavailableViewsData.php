<?php

namespace Drupal\consultancy_unavailable\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Consultancy unavailable entities.
 */
class ConsultancyUnavailableViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
