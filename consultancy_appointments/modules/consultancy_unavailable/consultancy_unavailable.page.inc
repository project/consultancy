<?php

/**
 * @file
 * Contains consultancy_unavailable.page.inc.
 *
 * Page callback for Consultancy unavailable entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Consultancy unavailable templates.
 *
 * Default template: consultancy_unavailable.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_consultancy_unavailable(array &$variables) {
  // Fetch ConsultancyUnavailable Entity Object.
  $consultancy_unavailable = $variables['elements']['#consultancy_unavailable'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
