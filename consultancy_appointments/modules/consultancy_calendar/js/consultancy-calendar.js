/**
 * @file
 * Consultancy Calendar View plugin JavaScript file.
 */




(function($, Drupal) {
  'use strict';

  Drupal.behaviors.consultancy_calendar = {
    attach: function(context, settings) {
      $(document).ready(function(){


        var response = '';
        $.ajax({ type: "GET",
          url: '/calendar-rest',
          async: false,
          success : function(text)
          {
            response = text;
          }
        });

        console.log(response);





        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

        console.log(date)
        scheduler.config.first_hour = 6;
        scheduler.config.last_hour = 14;
        scheduler.config.readonly = true;
        scheduler.init('scheduler_here', new Date(today.getFullYear(), today.getMonth(), today.getDate()), "month");
        scheduler.parse(response);

       /* scheduler.parse([
          {id: 1, start_date: "2019-04-15 09:00", end_date: "2019-04-15 12:00", text: "English lesson"},
          {id: 2, start_date: "2019-04-16 10:00", end_date: "2019-04-16 16:00", text: "Math exam"},
          {id: 3, start_date: "2019-04-16 10:00", end_date: "2019-04-21 16:00", text: "Science lesson"},
          {id: 4, start_date: "2019-04-17 16:00", end_date: "2019-04-17 17:00", text: "English lesson"},
          {id: 5, start_date: "2019-04-18 09:00", end_date: "2019-04-18 17:00", text: "Usual event"}
        ]);*/
      });

    }
  };
})(jQuery, Drupal);
