<?php

namespace Drupal\consultancy_calendar\Preprocessors\views;

class RowsArrayCreation {

  /**
   * @param array $variables
   *
   * @return array
   */
  public static function getFormattedArray(array $variables) {
    $returnArray = [];
    foreach ($variables as $id => $row) {
      $variables['rows'][$id]['view_row_entities'] = $row;
    }

    foreach ($variables['rows']['rows']['view_row_entities'] as $key => $item) {
      $returnArray[] = [
        'name' => $item->_entity->get('name')->getValue(),
        'date' => $item->_entity->get('date')->getValue(),
      ];

    }
    return $returnArray;

  }


}
