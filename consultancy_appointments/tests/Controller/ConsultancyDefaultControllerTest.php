<?php

namespace Drupal\consultancy\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the consultancy module.
 */
class ConsultancyDefaultControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "consultancy ConsultancyDefaultController's controller functionality",
      'description' => 'Test Unit for module consultancy and controller ConsultancyDefaultController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests consultancy functionality.
   */
  public function testConsultancyDefaultController() {
    // Check that the basic functions of module consultancy.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
