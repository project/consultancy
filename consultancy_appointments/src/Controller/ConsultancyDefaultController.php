<?php

namespace Drupal\consultancy\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ConsultancyDefaultController.
 */
class ConsultancyDefaultController extends ControllerBase {

  /**
   * Index.
   *
   * @return array
   *   Return Hello string.
   */
  public function index() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Consultancy give a customer the potential to ability to hire an entity for an amount of time this could be a person/s or a room')
    ];
  }



}
